package com.thisismartin.example.service;

import com.thisismartin.example.dto.CommentDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: martin.spasovski
 * Date: 6/3/13
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CommentsService {

    public List<CommentDTO> getAll();

    public CommentDTO add(CommentDTO updated);
}
