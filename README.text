           __   __        __   __             ___  __
     |\/| /  \ /  \ |\ | |  \ /  \ |  | |\ | |__  |__)
     |  | \__/ \__/ | \| |__/ \__/ |/\| | \| |___ |  \


An example web app showing Facebook's React JS library integrated with Thymeleaf and Spring MVC.
============

 Presentation
------------

This project is a ported version of [https://github.com/petehunt/react-tutorial](https://github.com/petehunt/react-tutorial) - a React tutorial which uses Node.js for the RESTful communication.
Almost everything is the same, this one just shows how it's done using Thymeleaf and Spring MVC in a clean and concise way.

For more, read the blog post: [Short intro on Web Components, React and an example with Spring MVC](http://blog.thisismartin.com/on-web-components-and-an-example-with-facebooks-react-slash-w-spring-mvc)

 Installation and running the app
------------

- Install [Maven 3](http://maven.apache.org/)
- Compile the project by running `mvn clean install`
- Run the project by executing `mvn tomcat7:run`
- Open browser and point to http://localhost:8090 and that's it!

 Feedback
------------
Is welcomed and greatly appreciated!

 License
------------
Copyright 2013 Martin Spasovski.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.